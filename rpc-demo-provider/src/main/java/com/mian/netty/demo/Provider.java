package com.mian.netty.demo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

public class Provider {

    public static void main(String[] args) throws IOException, InterruptedException {

        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext(new String[]{"application.xml"});
        context.start();
        System.in.read();
        Thread.sleep(Integer.MAX_VALUE);
    }
}
