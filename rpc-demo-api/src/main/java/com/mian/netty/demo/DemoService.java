package com.mian.netty.demo;

import com.mian.netty.demo.model.User;

public interface DemoService {

    String sayHello(String name);

    User getUser(String name);

    int getUserAge(String name);
}
