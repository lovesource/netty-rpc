package com.mian.netty.demo;

import com.mian.annotation.RemoteService;
import com.mian.netty.demo.model.User;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@RemoteService
public class DemoServiceImpl implements DemoService {

    static Map<String, User> userMap = new HashMap<>();
    static{
        userMap.put("lili", new User("lili", 22, 111.88));
        userMap.put("lucy", new User("lucy", 23, 667));
    }

    @Override
    public String sayHello(String name) {
        return "sayHello! " + name;
    }

    @Override
    public User getUser(String name) {
        return userMap.get(name);
    }

    @Override
    public int getUserAge(String name) {
        return userMap.get(name) == null? 0 : userMap.get(name).getAge();
    }
}
